#  Ansible Base Image

Installing the [official](https://raspi.debian.net/tested-images/) Debian distribution on Raspberry Pi devices, as an alternative to Raspberry Pi OS built with Ansible.

#### Caveats
The official Debian Raspberry Pi images don't ship with a user prebuilt like the Raspberry Pi OS `pi` user. 

The built images are shipped with a *passwordless root*, which means, you can log in as root as long as you do it locally _(either via a serial console or using a USB keyboard)._ However we replace the empty root password with a temporary hash so we can log in as our sysuser user and then `su -` because we need `python3-apt` for `apt-cache` and `sudo` to login in via `ansible` going forward.

The role also creates a system user without having to create one as root via console or keyboard as mentioned.

**NOTE:** Creating a system account or administering any accounts this way, is not advised nor recommended however the role automates a very boring process, especially if repeating this multiple times with no need to power up the Raspberry Pi initially.


[https://wiki.debian.org/RaspberryPi](https://wiki.debian.org/RaspberryPi)

#### Prerequisites
- [Balena Etcher](https://www.balena.io/etcher/) or alternative disk imaging tool for your Linux distribution
-  Raspberry Pi 4 4Gb or [Raspberry Pi Hardware](https://www.raspberrypi.org/products/) 
-  [San Disk](https://www.amazon.co.uk/SanDisk-Ultra-Flash-Drive-Read/dp/B07855LJ99/ref=sr_1_9?crid=KBXQ24EH28NG&dchild=1&keywords=usb+drive+128gb&qid=1614205606&sprefix=usb+drive%2Caps%2C176&sr=8-9) Ultra Fit USB Drive 128Gb or any USB drive above 4Gb.
- [Debian 10/11 (Buster/Bullseye Image](https://raspi.debian.net/tested-images) _(Debian 11/Bullseye images caused some boot issues using the San Disk USB Drives which will need further investigation.)_
- The Raspberry Pi 4 used was already flashed to support USB [booting](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/msd.md) beforehand, although you can still use a SD card for the process, it will be just slower.


#### Installation

Download and Burn the image using Etcher or alternate [tool](https://www.cyberciti.biz/faq/unix-linux-dd-create-make-disk-image-commands/) for your distro.
``` bash
$ git clone http://gitlab.com/planforfailure/ansible-image-base.git
```
Add your ssh public key to `files/ssh/publickey.pub`

Add your password hash to `tasks/create_account.yml` # line 12 using something like `printf "badpassword" | mkpasswd --stdin --method=sha-512`

## Licence

MIT/BSD
 
Edit `vars/vars.yml` replacing the following values
```yaml
user: hostuser # this is needed as the USB volume mounts on your host machine or equivalent using $USER or $LOGNAME \ 
/media/hostuser/RASPI{FIRM,ROOT} similar to Debian/Mint distros.
sysuser: sysuser
```
 
##### TL;DR 
Run the following which will create your `systemuser` account so you can login with your host machine public key. 
```bash
$ ansible-playbook deploy.yml
```

##### deploy.yml
- tasks/config_setup.yml
- tasks/create_account.yml
- post_task
   - umount `/media/hostuser/RASPIFIRM`
   - umount `/media/hostuser/RASPIROOT`

##### tasks/config_setup.yml 
- Confirms the volumes exist, play stops unless `true`
- Enables ssh
- Creates `/home/sysuser`
- Creates `/etc/sudoers.d/` on the volume
- Creates `/home/sysuser/.ssh` on the volume

##### tasks/create_account.yml
- Creates an entry in `/etc/passwd` on the volume
- Creates an entry in `/etc/shadow` on the volume
- Creates an entry in `/etc/group` on the volume
- Creates sudoers `NOPASSWD` entry in `/etc/sudoers.d/sysuser` on the volume
- Adds public key from `files/ssh/publickey.pub` to `/home/sysuser/.ssh/authorized_keys`

##### tasks/root.yml
- Replaces entry in `/etc/shadow` for root on the volume

#### If everything is successful
- Remove the USB drive
- Power on your Raspberry Pi
- Check out [ansible-post-install](https://gitlab.com/planforfailure/ansible-post-install)
 
```bash
$ ssh sysuser@raspberrypi.ip
$ su -  # changeme
apt update && apt upgrade -y && apt install sudo python3-apt -y  # python3-apt is required for apt-cache update
```
You can now run ansible against your Raspberry Pi

#### Something doesn't work?
Pull requests and issues are welcome!

## Licence

MIT/BSD

